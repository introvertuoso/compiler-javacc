package nodes;

public class ForNode extends AbstractTreeNode {

	String varName;

	@Override
	public String toString()
	{
		return "For " + varName;
	}

	@Override
	public Object execute(Context context) {
		// TODO everything here has become integers\
		// TODO do a quick run over
		context.startScope();

		int start = (Integer)children.get(0).execute(context);
		int end = (Integer)children.get(1).execute(context);
		int step = 1;
		if (children.size() > 3)
			step = (Integer)children.get(3).execute(context);
		double current = start;
		while(true){
			if (step < 0 && current < end)
				break;
			if (step > 0 && current > end)
				break;
			context.getVars().put(varName, current);
			context.getVarNames().add(varName);
			children.get(2).execute(context);
			end = (Integer)children.get(1).execute(context);
			if (children.size() > 3)
				step = (Integer)children.get(3).execute(context);
			current += step;
		}

		context.endScope();

		return null;
	}

	@Override
	public Object convert(Context context) {
		
		return "for(" + varName + "=" + children.get(0).convert(context)
				+";" + varName + "<=" + children.get(1).convert(context)
				+ ";" + varName + "+=" + (children.size() > 3? children.get(3).convert(context) : "1")
				+ ")\r\n{" + children.get(2).convert(context) + "}" ;
	}

	// Getters and Setters

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

}
