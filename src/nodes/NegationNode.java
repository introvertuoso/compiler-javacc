package nodes;

public class NegationNode extends MajorConditionNode {

    @Override
    public Object execute(Context context) {
        Boolean b = (Boolean) children.get(0).execute(context);
        return !b;
    }

    @Override
    public Object convert(Context context) {
        return null;
    }
}
