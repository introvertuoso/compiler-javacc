package nodes;

import java.util.ArrayList;

public abstract class AbstractTreeNode {

	protected String name;
	protected ArrayList<AbstractTreeNode> children = new ArrayList<AbstractTreeNode>();

	protected Object Object2Type(String type, Object value) {
		if(type.equals("int"))
			return (Integer)value;
		else if (type.equals("Double"))
			return (Double)value;
		else if(type.equals("String"))
			return (String)value;
		else if(type.equals("char"))
			return (Character)value;
		else
			return null;
	}
	public abstract Object execute(Context context) ;
	public abstract Object convert(Context context);

	public void print(String prefix) {
		System.out.println(prefix + this);
		for(AbstractTreeNode n : children)
			n.print(prefix + "---");
	}

	// Getters and Setters
	protected String checkForDigitOrDots(String value) {
		char[] temp = value.toCharArray();
		int dotsCounter = 0;
		int digitsCounter = 0;
		for (char c:temp) {
			if(c == '.' ){
				dotsCounter++;
			}
			else if(Character.isDigit(c)) {
				digitsCounter++;
			}
		}
		if(dotsCounter == 1 && digitsCounter == temp.length-1)
			return "double";
		else if (dotsCounter == 0 && digitsCounter == temp.length)
			return "int";
		else
			return "String";
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<AbstractTreeNode> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<AbstractTreeNode> children) {
		this.children = children;
	}

	public void addChild(AbstractTreeNode child) {
		children.add(child);
	}
}
