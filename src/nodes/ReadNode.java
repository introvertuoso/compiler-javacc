package nodes;

import java.util.Scanner;

public class ReadNode extends AbstractTreeNode {


    @Override
    public String toString() {
        return "Read " + varName;
    }

    String varName;

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    @Override
    public Object execute(Context context) {
        if (context.getVars().containsKey(varName)) {
            System.out.println("Variable will be overridden value and type wise.");
            System.out.println("(int = ([\"0\"-\"9\"])+");
            System.out.println(" double = (()+\".\"()*) | (()*\".\"()+)");
            System.out.println(" String = (~[\"\\n\"])*)");
            System.out.println(" char = String of length=1");
            Scanner sc = new Scanner(System.in);
            String s = sc.nextLine();
            if (context.getVars().get(varName).getClass() == String.class) {
                context.getVars().put(varName, s);
                context.getVarNames().add(varName);
            } else if (context.getVars().get(varName).getClass() == Character.class && s.length() == 1) {
                context.getVars().put(varName, s.charAt(0));
                context.getVarNames().add(varName);
            } else {
                int dotCounter = 0, digitCounter = 0;
                for (char c : s.toCharArray()) {
                    if (c == '.') {
                        dotCounter++;
                    } else if (Character.isDigit(c)) {
                        digitCounter++;
                    }
                }
                if (dotCounter == 1 && digitCounter == s.length() - 1 && s.length() > 1) {
                    context.getVars().put(varName, Double.parseDouble(s));
                    context.getVarNames().add(varName);
                } else if (digitCounter == s.length() && s.length() > 0) {
                    context.getVars().put(varName, Integer.parseInt(s));
                    context.getVarNames().add(varName);
                } else {
                    if (s.length() == 1) {
                        context.getVars().put(varName, s.charAt(0));
                        context.getVarNames().add(varName);
                    } else {
                        context.getVars().put(varName, (String) s);
                        context.getVarNames().add(varName);
                    }
                }
            }
        } else {
            // TODO maybe make it do?
            System.out.println(varName + " not found in this scope");
        }
        return null;
    }

    @Override
    public Object convert(Context context) {
        return "cin >> " + varName + ";";
    }

}
