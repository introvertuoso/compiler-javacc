package nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Context {

    HashMap<String, Object> vars = new HashMap<>();
	Stack<HashMap<String, Object>> varStack = new Stack<HashMap<String, Object>>();
    ArrayList<String> varNames = new ArrayList<>();
	Stack<ArrayList<String>> varNameStack = new Stack<>();

    public void startScope() {
        varStack.push(vars);
        varNameStack.push(varNames);

//        HashMap<String, Object> tmp = new HashMap<String, Object>();
//        ArrayList<String> tmpN = new ArrayList<String>();
//        for (int i = 0; i < varNames.size(); i++) {
//            tmp.put(varNames.get(i), vars.get(varNames.get(i)));
//            tmpN.add(varNames.get(i));
//        }

        vars = new HashMap<String, Object>(varStack.peek());
        varNames = new ArrayList<String>(varNameStack.peek());

//        vars.putAll(tmp);
//        varNames.addAll(tmpN);
    }

    public void startFunctionScope() {
        varStack.push(vars);
        varNameStack.push(varNames);

        vars = new HashMap<String, Object>();
        varNames = new ArrayList<String>();
    }

    public void endScope() {
        for (String s : varNames) {
            if (varNameStack.peek().contains(s)) {
                varStack.peek().put(s, vars.get(s));
            }
        }

        varNames = varNameStack.pop();
        vars = varStack.pop();
    }

    public void endFunctionScope() {
        varNames = varNameStack.pop();
        vars = varStack.pop();
    }

    // Getters and Setters

    public HashMap<String, Object> getVars() {
        return vars;
    }

    public void setVars(HashMap<String, Object> vars) {
        this.vars = vars;
    }

    public Stack<HashMap<String, Object>> getVarStack() {
        return varStack;
    }

    public void setVarStack(Stack<HashMap<String, Object>> varStack) {
        this.varStack = varStack;
    }

    public ArrayList<String> getVarNames() {
        return varNames;
    }

    public void setVarNames(ArrayList<String> varNames) {
        this.varNames = varNames;
    }
}
