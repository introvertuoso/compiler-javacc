package nodes;

import java.util.ArrayList;
import java.util.HashMap;

public class FuncCallNode extends ExpressionNode {

	String functionName;
	HashMap<String, FunctionDef> functions = new HashMap<String, FunctionDef>();

	@Override
	public Object execute(Context context) {
		
		if (!functions.containsKey(functionName)) {
			throw new RuntimeException(functionName + " not found in this scope");
		}

		FunctionDef f = functions.get(functionName);
		if (f.getParams().size() != this.children.size()) {
			throw new RuntimeException("Invalid params");
		}

		HashMap<String, Object> tmp = new HashMap<String, Object>();
		ArrayList<String> tmpN = new ArrayList<>();
		for (int i=0; i < f.getParams().size(); i++) {
			tmp.put(f.getParams().get(i), this.getChildren().get(i).execute(context));
			tmpN.add(f.getParams().get(i));
		}

		context.startFunctionScope();

		context.getVars().putAll(tmp);
		context.getVarNames().addAll(tmpN);

//		System.out.println(context.getVars());
//		System.out.println(context.getVarNames());
		f.root.execute(context);
		Object res = context.getVars().get("ret");

		context.endFunctionScope();

		return res;
	}

	@Override
	public Object convert(Context context) {
		return null;
	}

	// Getters and Setters

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public HashMap<String, FunctionDef> getFunctions() {
		return functions;
	}

	public void setFunctions(HashMap<String, FunctionDef> functions) {
		this.functions = functions;
	}

}
