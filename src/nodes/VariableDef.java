package nodes;

public class VariableDef extends AbstractTreeNode{
    String varName, type;

    @Override
    public Object execute(Context context) {
        if (!context.getVars().containsKey(varName)) {
            switch (type) {
                case "int":
                    context.getVars().put(varName, (Integer) 0);
                    context.getVarNames().add(varName);
                    break;
                case "char":
                    context.getVars().put(varName, (Character) ' ');
                    context.getVarNames().add(varName);
                    break;
                case "String":
                    context.getVars().put(varName, (String) " ");
                    context.getVarNames().add(varName);
                    break;
                case "double":
                    context.getVars().put(varName, (Double) 0.0);
                    context.getVarNames().add(varName);
                    break;
            }
        } else {
            System.out.println("Variable already defined");
        }
        return null;
    }

    @Override
    public Object convert(Context context) {
        return null;
    }

    // Getters and Setters

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
