package nodes;

public class ConditionNode extends MajorConditionNode {

    String operator;

    @Override
    public Object execute(Context context) {
		Object d1 = children.get(0).execute(context);
        Object d2 = children.get(1).execute(context);

        if ((d1.getClass() == Integer.class && d2.getClass() == Integer.class)) {
            switch(operator) {
                case ">":
                    return (Integer)d1 > (Integer)d2;
                case "<":
                    return (Integer)d1 < (Integer)d2;
                case ">=":
                    return (Integer)d1 >= (Integer)d2;
                case "<=":
                    return (Integer)d1 <= (Integer)d2;
                case "<>":
                    return (Integer)d1 != (Integer)d2;
                case "=":
                    return (Integer)d1 == (Integer)d2;
            }
        } else if (d1.getClass() == Double.class && d2.getClass() == Double.class) {
            switch(operator) {
                case ">":
                    return (Double)d1 > (Double)d2;
                case "<":
                    return (Double)d1 < (Double)d2;
                case ">=":
                    return (Double)d1 >= (Double)d2;
                case "<=":
                    return (Double)d1 <= (Double)d2;
                case "<>":
                    return (Double)d1 != (Double)d2;
                case "=":
                    return (Double)d1 == (Double)d2;
            }
        } else if (d1.getClass() == Integer.class && d2.getClass() == Double.class) {
            switch(operator) {
                case ">":
                    return (Integer)d1 > (Double)d2;
                case "<":
                    return (Integer)d1 < (Double)d2;
                case ">=":
                    return (Integer)d1 >= (Double)d2;
                case "<=":
                    return (Integer)d1 <= (Double)d2;
                case "<>":
                    return (Double)d1 != (Double)d2;
                case "=":
                    return (Double)d1 == (Double)d2;
            }
        } else if (d1.getClass() == Double.class && d2.getClass() == Integer.class) {
            switch(operator) {
                case ">":
                    return (Double)d1 > (Integer)d2;
                case "<":
                    return (Double)d1 < (Integer)d2;
                case ">=":
                    return (Double)d1 >= (Integer)d2;
                case "<=":
                    return (Double)d1 <= (Integer)d2;
                case "<>":
                    return (Double)d1 != (Double)d2;
                case "=":
                    return (Double)d1 == (Double)d2;
            }
		}
        // TODO here to one can explore further combinations
		return null;
    }

    @Override
    public Object convert(Context context) {
        String op = operator;
        if (op.equals("="))
            op = "==";
        if (op.equals("<>"))
            op = "!=";
        return children.get(0).convert(context) + op
                + children.get(1).convert(context);
    }

    @Override
    public String toString() {
        return "Condition " + operator;
    }

    // Getters and Setters

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

}
