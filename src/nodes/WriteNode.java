package nodes;

public class WriteNode extends AbstractTreeNode {

	@Override
	public String toString()
	{
		return "Write";
	}
	
	@Override
	public Object execute(Context context) {
		// We have no null variables.
		if (children.get(0).execute(context) != null) {
			System.out.println(children.get(0).execute(context));
		}
		return null;
	}

	@Override
	public Object convert(Context context) {
		return "cout << " + children.get(0).convert(context) + " << endl;";
	}

}
