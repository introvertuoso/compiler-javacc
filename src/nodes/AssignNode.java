package nodes;

public class AssignNode extends AbstractTreeNode {

	String varName;

	@Override
	public String toString()
	{
		return "Assign " + varName;
	}

	@Override
	public Object execute(Context context) {
//		System.out.println("Assign" + children.get(0).toString());

		Object res = children.get(0).execute(context);
		Class c = res.getClass();

		if (c == Integer.class) {
			if (!context.getVars().containsKey(varName)) {
				context.getVars().put(varName, (Integer) res);
				context.getVarNames().add(varName);
			} else {
				if (res.getClass().toString().equals(
						context.getVars().get(varName).getClass().toString())) {
					context.getVars().put(varName, (Integer) res);
					context.getVarNames().add(varName);
				} else {
					System.out.println("Incompatible types");
				}
			}
		} else if (c == Character.class) {
			if (!context.getVars().containsKey(varName)) {
				context.getVars().put(varName, (Character) res);
				context.getVarNames().add(varName);
			} else {
				if (res.getClass().toString().equals(
						context.getVars().get(varName).getClass().toString())) {
					context.getVars().put(varName, (Character) res);
					context.getVarNames().add(varName);
				} else {
					System.out.println("Incompatible types");
				}
			}
		} else if (c == String.class) {
			if (!context.getVars().containsKey(varName)) {
				context.getVars().put(varName, (String) res);
				context.getVarNames().add(varName);
			} else {
				if (res.getClass().toString().equals(
						context.getVars().get(varName).getClass().toString())) {
					context.getVars().put(varName, (String) res);
					context.getVarNames().add(varName);
				} else {
					System.out.println("Incompatible types");
				}
			}
		} else if (c == Double.class) {
			if (!context.getVars().containsKey(varName)) {
				context.getVars().put(varName, (Double) res);
				context.getVarNames().add(varName);
			} else {
				if (res.getClass().toString().equals(
						context.getVars().get(varName).getClass().toString())) {
					context.getVars().put(varName, (Double) res);
					context.getVarNames().add(varName);
				} else {
					System.out.println("Incompatible types");
				}
			}
		}
		return null;
	}

	@Override
	public Object convert(Context context) {
		return varName + "=" + children.get(0).convert(context) + ";";
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

}
