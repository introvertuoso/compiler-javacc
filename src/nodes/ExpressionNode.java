package nodes;

public class ExpressionNode extends AbstractTreeNode {

    @Override
    public String toString() {
        return "Expression " + operator;
    }

    String operator;

    @Override
    public Object execute(Context context) {
        Object d1 = children.get(0).execute(context);
        Object d2 = children.get(1).execute(context);

        if (d1 != null && d2 != null) {
            if ((d1.getClass() == Integer.class && d2.getClass() == Integer.class)) {
                switch (operator) {
                    case "+":
                        return (Integer) d1 + (Integer) d2;
                    case "-":
                        return (Integer) d1 - (Integer) d2;
                    case "*":
                        return (Integer) d1 * (Integer) d2;
                    case "/":
                        return (Integer) d1 / (Integer) d2;
                }
            } else if (d1.getClass() == Double.class && d2.getClass() == Double.class) {
                switch (operator) {
                    case "+":
                        return (Double) d1 + (Double) d2;
                    case "-":
                        return (Double) d1 - (Double) d2;
                    case "*":
                        return (Double) d1 * (Double) d2;
                    case "/":
                        return (Double) d1 / (Double) d2;
                }
            } else if (d1.getClass() == Integer.class && d2.getClass() == Double.class) {
                switch (operator) {
                    case "+":
                        return (Integer) d1 + (Double) d2;
                    case "-":
                        return (Integer) d1 - (Double) d2;
                    case "*":
                        return (Integer) d1 * (Double) d2;
                    case "/":
                        return (Integer) d1 / (Double) d2;
                }
            } else if (d1.getClass() == Double.class && d2.getClass() == Integer.class) {
                switch (operator) {
                    case "+":
                        return (Double) d1 + (Integer) d2;
                    case "-":
                        return (Double) d1 - (Integer) d2;
                    case "*":
                        return (Double) d1 * (Integer) d2;
                    case "/":
                        return (Double) d1 / (Integer) d2;
                }
            } else if (d1.getClass() == String.class && d2.getClass() == String.class) {
                switch (operator) {
                    case "+":
                        return (String) d1 + (String) d2;
                }
            }

            // TODO here one may add char related stuff
			// TODO maybe even further combinations

        } else {
            return children.get(0).execute(context);
        }
        return null;
    }

    @Override
    public Object convert(Context context) {
        return children.get(0).convert(context) + operator
                + children.get(1).convert(context);
    }

    // Getters and Setters

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

}
