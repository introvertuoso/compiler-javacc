package nodes;

public class MajorConditionNode extends AbstractTreeNode{

    String operator;

    @Override
    public Object execute(Context context) {

        Boolean b1 = (Boolean) children.get(0).execute(context);
        Boolean b2 = (Boolean) children.get(1).execute(context);
        switch (operator) {
            case "$":
                return b1 & b2;
            case "|":
                return b1 | b2;
        }
        return null;
    }

    @Override
    public Object convert(Context context) {
        return null;
    }

    @Override
    public String toString() {
        return "MajorCondition " + operator;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
