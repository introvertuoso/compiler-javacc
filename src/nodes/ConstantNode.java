package nodes;

public class ConstantNode extends ExpressionNode {
	
	Object value;

	@Override
	public String toString()
	{
		return value + "";
	}

	@Override
	public Object execute(Context context) {
		return value;
	}

	@Override
	public Object convert(Context context) {
		return value + "";
	}

	// Getters and Setters

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
